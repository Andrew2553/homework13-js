// 1. setTimeout() вызывает функцию один раз после заданной задержки и останавливается. Он выполняет код только один раз и не повторяется автоматически. 
//    setInterval() вызывает функцию периодически через заданный интервал времени. Он продолжает вызывать функцию с указанной задержкой до тех пор, пока не будет явно остановлен с помощью clearInterval().


// 2. Когда в функцию setTimeout() передается нулевая задержка (setTimeout(callback, 0)), она не сработает мгновенно. Вместо этого, функция будет поставлена в очередь выполнения и будет выполнена как можно скорее, но только после завершения текущих задач в JavaScript.

// 3. Вызов функции clearInterval() является важным, когда вам больше не нужен ранее созданный цикл запуска, работающий через setInterval(). Это важно по нескольким причинам:

// Остановка выполнения: Если вы не вызовете clearInterval(), цикл setInterval() будет продолжать запускать указанную функцию через заданный интервал, что может привести к непредсказуемым последствиям и неисправностям в программе. Это особенно важно, когда вы используете циклы для обновления данных, анимации или взаимодействия с пользователем.

// Экономия ресурсов: Продолжение работы цикла setInterval() без необходимости может потреблять дополнительные ресурсы, такие как память и процессорное время. Если вам не нужно продолжать выполнение цикла, остановка его работы с помощью clearInterval() позволяет экономить ресурсы вашей программы.

// Избежание конфликтов: Если вы создаете новый цикл setInterval() с той же переменной идентификатора (например, без вызова clearInterval()), то новый цикл будет конфликтовать с предыдущим. Это может приводить к непредсказуемым результатам и некорректной работе вашей программы.

// Поэтому важно всегда помнить вызывать clearInterval() после завершения работы с циклом setInterval(), чтобы обеспечить правильную и эффективную работу вашей программы.




document.addEventListener("DOMContentLoaded", function () {
  const images = document.querySelectorAll(".image-to-show");
  const stopButton = document.getElementById("stopButton");
  const resumeButton = document.getElementById("resumeButton");
  const timer = document.getElementById("timer");

  let currentIndex = 0;
  let intervalId = null;
  let countdownId = null;
  let remainingTime = 3;
  let milliseconds = 0;

  function fade(element, duration, targetOpacity) {
    let opacity = targetOpacity === 1 ? 0 : 1;
    const interval = 10;
    const delta = (targetOpacity - opacity) / (duration / interval);

    const fadeInterval = setInterval(() => {
      if ((targetOpacity === 1 && opacity >= 1) || (targetOpacity === 0 && opacity <= 0)) {
        clearInterval(fadeInterval);
        element.style.opacity = targetOpacity;
        element.classList.toggle("active", targetOpacity === 1);
      } else {
        opacity += delta;
        element.style.opacity = opacity;
      }
    }, interval);
  }

  function showImage(index) {
    const currentImage = images[currentIndex];
    const nextImage = images[index];

    fade(currentImage, 500, 0);
    fade(nextImage, 500, 1);

    currentIndex = index;
  }

  function startSlideshow() {
    const nextIndex = (currentIndex + 1) % images.length;
    showImage(nextIndex);
    resetTimer();
  }

  function stopSlideshow() {
    clearInterval(intervalId);
    clearInterval(countdownId);
    timer.classList.add("hidden");
    stopButton.classList.add("hidden");
    resumeButton.classList.remove("hidden");
  }

  function resumeSlideshow() {
    intervalId = setInterval(startSlideshow, 3000);
    startTimer();
    timer.classList.remove("hidden");
    stopButton.classList.remove("hidden");
    resumeButton.classList.add("hidden");
  }

  function resetTimer() {
    remainingTime = 3;
    milliseconds = 0;
    updateTimerDisplay();
  }

  function updateTimerDisplay() {
    timer.textContent = `${remainingTime}.${padMilliseconds(milliseconds)} секунд`;
  }

  function startTimer() {
    milliseconds -= 100;
    if (milliseconds < 0) {
      milliseconds = 900;
      remainingTime -= 1;
    }
    updateTimerDisplay();
  }

  function padMilliseconds(value) {
    return value.toString().padStart(3, "0");
  }

  stopButton.addEventListener("click", stopSlideshow);
  resumeButton.addEventListener("click", resumeSlideshow);

  // Hide the original stop button
  stopButton.classList.add("hidden");

  intervalId = setInterval(startSlideshow, 3000);
  countdownId = setInterval(startTimer, 100);

  // Optional: Timer display
  updateTimerDisplay();
});
